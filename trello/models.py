from django.db import models


class TrelloList(models.Model):
    list_id = models.CharField(max_length=32)
    name = models.CharField(max_length=100)
    closed = models.BooleanField()
    board_id = models.CharField(max_length=32)


class TrelloCard(models.Model):
    list_id = models.ForeignKey(to='TrelloList', on_delete=models.CASCADE)
    card_id = models.CharField(max_length=32)
    description = models.TextField()
    attachemnt = models.FileField(upload_to='cards/attachemnts')
    subject_email = models.EmailField(null=True)

