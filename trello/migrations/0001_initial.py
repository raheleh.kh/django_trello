# Generated by Django 5.0.4 on 2024-04-17 08:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TrelloList',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('list_id', models.CharField(max_length=32)),
                ('name', models.CharField(max_length=100)),
                ('closed', models.BooleanField()),
                ('board_id', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='TrelloCard',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card_id', models.CharField(max_length=32)),
                ('description', models.TextField()),
                ('attachemnt', models.FileField(upload_to='cards/attachemnts')),
                ('subject_email', models.EmailField(max_length=254, null=True)),
                ('list_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trello.trellolist')),
            ],
        ),
    ]
