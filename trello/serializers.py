from rest_framework.serializers import ModelSerializer

from trello.models import TrelloList, TrelloCard

class ListSerializer(ModelSerializer):
    class Meta:
        model = TrelloList
        fields = '__all__'

class CardSerializer(ModelSerializer):
    class Meta:
        model = TrelloCard
        fields = '__all__'