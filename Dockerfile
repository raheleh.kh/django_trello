FROM python:latest
WORKDIR /app
COPY ./requirements.txt /app/
RUN pip install -U pip 
RUN pip install -r requirements.txt
COPY . $DockerHome
COPY ./entrypoint.sh /app/
ENTRYPOINT ["sh", "entrypoint.sh"]