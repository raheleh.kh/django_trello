from django.contrib import admin
from trello.models import TrelloList, TrelloCard
admin.site.register(TrelloList)
admin.site.register(TrelloCard)

