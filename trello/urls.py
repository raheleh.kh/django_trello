from django.urls import path
from . import views

urlpatterns = [
    path('list', views.TrelloListView.as_view()),
    path('card', views.TrelloCardView.as_view())
]
